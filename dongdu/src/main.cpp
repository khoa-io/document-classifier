﻿/**
 * @file main.cpp Export C/C++ classes, functions to use in Python
 * @author Hoàng Văn Khoa <hoangvankhoa@outlook.com>
 *
 */
#include <iostream>
#include "Machine.h"
#include <Python.h>


static PyObject * demo_function(PyObject *self, PyObject *args)
{
	// Tìm hiểu về dongdu tại đây
	using std::cout;
	cout << "Hello World\n";	
	return PyLong_FromLong(10);
}


static struct PyMethodDef dongdu_methods[] =
{
	{
		"demo_function",
		(PyCFunction)demo_function,
		METH_VARARGS,
		0
	},

	{ 0, 0, 0 }
};


static struct PyModuleDef dongdu_module = {
	PyModuleDef_HEAD_INIT,
	"dongdu",				/* m_name */
	"This is a module",		/* m_doc */
	-1,						/* m_size */
	dongdu_methods,			/* m_methods */
	NULL,					/* m_reload */
	NULL,					/* m_traverse */
	NULL,					/* m_clear */
	NULL,					/* m_free */
};

PyMODINIT_FUNC PyInit_dongdu(void)
{
	PyObject *m;
	m = PyModule_Create(&dongdu_module);
	if (m == NULL)
		return NULL;
	return m;
}
