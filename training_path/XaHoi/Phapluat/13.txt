Thêm tội danh cáo buộc với nam sinh sát hại nghệ sĩ Đỗ Linh
Sau ân ái, nhân lúc nghệ sĩ Đỗ Linh quay vào nhà tắm, Bảo lấy dao đâm vào lưng bạn tình hòng cướp tài sản.
Nghệ sĩ Đỗ Linh từng bị bạn tình đồng tính tổ chức cướp hụt  /  Nghi can sát hại nghệ sĩ Đỗ Linh là nam sinh 15 tuổi
Ngày 5/2, Công an TP HCM đã khởi tố, bắt giam Nguyễn Công Bảo (15 tuổi) để điều tra về hành vi Cướp tài sản, bên cạnh tội Giết người đã truy cứu trách nhiệm hình sự từ trước. Nạn nhân bị Bảo sát hại, cướp tài sản là nghệ sĩ cải lương Đỗ Linh (43 tuổi). Liên quan vụ án, bạn học của Bảo là Trần Hà Bảo Toàn (16 tuổi) bị điều tra về hành vi Cướp tài sản với vai trò đồng phạm.
Theo điều tra, Bảo mồ côi cha mẹ, quen Đỗ Linh từ tháng 10/2013 và thường xuyên quan hệ đồng tính. Sau thời gian dài không liên lạc, giữa tháng 1, nghi can được nam nghệ sĩ gọi điện rủ đi chơi và hứa cho tiền. Nam sinh lớp 9 kể cho Toàn nghe và cả hai lên kế hoạch cướp tài sản.
Hẹn nghệ sĩ cải lương tại công viên khu Trung Sơn (quận 8), Bảo thủ dao nấp vào gốc cây chờ "con mồi" trong khi Toàn lảng vảng gần đấy sẵn sàng hỗ trợ. Tuy nhiên, kế hoạch của hai cậu học trò bất thành do Đỗ Linh đến nơi không thấy Bảo nên đã bỏ đi.
Nạn nhân là nghệ sĩ cải lương.
Hôm sau, Đỗ Linh gọi điện trách Bảo đã cho mình “leo cây” và đề nghị gặp nhau vào tối 26/1. Do thời điểm đó Toàn đi chơi với bạn gái nên Bảo một mình thực hiện kế hoạch. Đứng chờ ở cầu Kênh Xáng (huyện Bình Chánh), cậu học trò được nam nghệ sĩ đến đón và chở về căn nhà thuê trong con hẻm ở đường Hưng Phú, sát cầu Chánh Hưng (phường 10, quận 8).
Tại đây, sau khi ân ái, thừa lúc Đỗ Linh quay vào nhà tắm, Bảo lấy dao đâm vào lưng nạn nhân. Trong lúc giằng co, nam sinh bị thương ở tay còn nạn nhân ngã từ trên gác xuống đất.
Khi hàng xóm nghe tiếng kêu cứu kéo đến phá cửa, Bảo hoảng sợ leo qua mái tôn phía sau nhà rồi thoát ra đường Chánh Hưng. Nạn nhân Đỗ Linh được xác định tử vong tại hiện trường. Trên đường Bảo bỏ trốn, máu từ vết thương vương khắp nơi.
Chạy thoát về nhà, Bảo gọi cho Toàn kể mọi chuyện, nhờ đến băng bó vết thương. Do vết cắt sâu, máu không ngừng chảy nên nam sinh vào bệnh viện điều trị và bị cảnh sát phát hiện.
Vụ việc đang được điều tra mở rộng.