Rủ người yêu nhí đi trộm tài sản
Làm quen bé gái 15 tuổi qua Facebook, sau những lần quan hệ tình dục, Quang rủ người yêu nhí đi trộm tài sản.
Ngày 4/2, Công an TP Huế (Thừa Thiên - Huế) đã bắt Nguyễn Văn Quang (18 tuổi) để điều tra hành vi giao cấu với trẻ em và trộm cắp tài sản. 
Theo điều tra, Quang quen bé Quỳnh (15 tuổi) qua Facebook. Trong gần 2 tháng thân thiết, cậu ta nhiều đưa Quỳnh về nhà hoặc vào nhà nghỉ để quan hệ tình dục. Do thiếu tiền ăn chơi, mua sắm, Quang rủ người yêu nhí cùng đi trộm tài sản.
Mới đây, cặp đôi đến nhà bạn Quang ở đường Hàn Mạc Tử (phường Vĩ Dạ, TP Huế) chơi, rình sơ hở của gia chủ lấy trộm máy tính bảng và laptop. Đang trên đường lẩn trốn cùng tang vật, cả hai bị chủ nhà phát hiện, bắt giao công an.
Hiện, Quỳnh được tại ngoại. Công an TP Huế xác định Quang có tiền án về tội Trộm cắp tài sản, đang thử thách thì tái phạm.