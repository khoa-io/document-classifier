'Bà đầm thép' giúp Chelsea thắng lớn trên sàn chuyển nhượng
Marina Granovskaia, trợ lý cấp cao của tỷ phú Abramovich, chính là người góp công lớn trong những thương vụ mua bán khôn ngoan tại Chelsea thời gian gần đây.
Chelsea vừa tậu 'Vespa', vừa xuất sắc giải bài toán tài chính / Granovskaia - Người phụ nữ quyền lực nhất thế giới bóng đá
Mùa giải này, Chelsea đã thực hiện được ba vụ chiêu mộ đáng chú ý là Fabregas, Costa và Cuadrado, đồng thời tiếp tục có lãi trên thị trường chuyển nhượng cầu thủ, khác hẳn với thời kỳ Roman Abramovich mới đầu tư vào CLB này.
Đứng sau sự thay đổi kỳ diệu này ở CLB thành London là người phụ nữ quyền lực nhất làng bóng đá Anh hiện nay - Marina Granovskaia. Bà được coi là cánh tay phải của ông chủ tỷ phú người Nga, và hiện chịu trách nhiệm chính trong tất cả những vụ mua bán cầu thủ của Chelsea.
Chi tiền tấn mua Cuadrado, nhưng Chelsea vẫn thoải mái đáp ứng Đạo luật Cân bằng Tài chính nhờ bán được với giá cao những cầu thủ không có nhiềugiá trị chuyên môn dưới trướng Mourinho như Schurrle hay Bertrand. Ảnh: Mirror.
Trước khi công khai hiện diện ở sân Stamford Bridge để giúp Chelsea cân đối tài chính và chỉ đạo các vụ đàm phán chuyển nhượng cầu thủ, Marina Granovskaia đã là cố vấn đáng tin cậy của Abramovich trong khoảng 17 năm qua.
Chỉ trong ngày cuối của phiên chợ đông 2/2/2015, Chelsea đã nhanh gọn thu về khoản lợi nhuận 10,5 triệu đôla trong khi vẫn có thêm một tân binh được đánh giá cao là Juan Cuadrado - cầu thủ đắt giá nhất mùa đông châu Âu 2015. Việc bán được Andre Schurrle (36 triệu đôla) và Ryan Bertrand (15 triệu đôla) với giá hợp lý đã giúp họ có thêm một kỳ chuyển nhượng thành công, ít nhất là về mặt tài chính.
Trước đó, kỳ chuyển nhượng mùa hè 2014 cũng diễn ra tuyệt vời với Chelsea, khi họ mang về hai ngôi sao người Tây Ban Nha là tiền đạo Diego Costa và nhạc trưởng Cesc Fabegras, trong khi thu lại khoản tiền không nhỏ từ vụ bán David Luiz và Juan Mata.
Chelsea chi tới 187 triệu đôla trong mùa giải hiện tại, nhưng họ vẫn kiếm được chút lãi thông qua 194 triệu đôla từ sự ra đi của các cầu thủ không còn trong kế hoạch phát triển đội bóng. Và Granovskaia có vai trò quyết định trong hầu hết các thương vụ lớn. Người phụ nữ 39 tuổi mang hai quốc tịch Nga và Canada này là một trong ba giám đốc được Chelsea công khai trên trang web chính thức của CLB.
"Nghiêm khắc, mạnh mẽ, công bằng, xinh đẹp và luôn rất khôn ngoan khi xử lý công việc tại Chelsea", một đại diện của CLB nhận xét về Granovskaia.
Bà tham gia vào hoạt động tài chính của Chelsea từ nhiều năm qua. Và sau khi Ron Gourlay rời ghế Tổng giám đốc hồi năm ngoái, Granovskaia đã giữ vai trò tích cực và lớn hơn nhiều tại CLB này cùng với Chủ tịch Bruce Buck.
Bà tốt nghiệp Đại học quốc gia Moscow năm 1997, và cùng năm đó bắt đầu làm việc cho Sibneft, công ty dầu khí nổi tiếng từng thuộc quyền sở hữu của Roman Abramovich. Bà là cố vấn cao cấp cho tỷ phú người Nga suốt 17 năm qua, chịu trách nhiệm quản lý khối tài sản lớn và các lợi ích khác của Abramovich.
Marina là người tâm phúc của Abramovich từ rất lâu trước khi ngồi vào ghế Giám đốc Chelsea, phụ trách chuyển nhượng. Ảnh: CFC.
Bà chuyển từ thủ đô nước Nga tới sống và làm việc ở London không lâu sau khi Abramovich mua lại CLB Chelsea hồi năm 2003. Kể từ 2010, bà chính thức hoạt động như một vị đại diện của ông chủ người Nga trong các vấn đề liên quan tới Chelsea, đồng thời hỗ trợ ban giám đốc CLB. Từ tháng 6/2013, bà gia nhập ban lãnh đạo Chelsea.
Với sự góp mặt của Granovskaia, Chelsea có một đội ngũ những nhà đàm phán bậc thày. Tuyển thủ Đức Andre Schurrle ký hợp đồng trị giá 27 triệu đôla với Chelsea hồi năm 2013. Anh chỉ có tên trong đội hình xuất phát của CLB này 20 trận, và hoàn toàn không được HLV Mourinho tin tưởng. Nhưng bằng cách nào đó, Chelsea mới đây vẫn bán được anh cho Wolfsburg với giá 36 triệu đôla. Khoản này cộng với 15 triệu đôla từ vụ bán Ryan Bertrand tới Southampton cho phép Jose Mourinho được chi 40,5 triệu đôla để tuyển mộ Juan Cuadrado từ Fiorentina. Cầu thủ chạy cánh người Colombia là người mà nhà cầm quân Bồ Đào Nha thực sự muốn có sau khi chứng kiến anh tỏa sáng ở World Cup 2014.
Trước đợt mua bán cầu thủ được đánh giá khôn ngoan lần này, Chelsea cũng đã phát triển được thói quen bán đi những cầu thủ với mức giá tốt nhất có thể, trong đó vụ bán David Luiz với 76 triệu đôla cho PSG là một ví dụ điển hình. Tuyển thủ Brazil này trở thành hậu vệ đắt giá nhất thế giới.
Chỉ tính riêng trong khoảng 12 tháng qua, Chelsea đã bán được tới bốn cầu thủ mà Mourinho coi là người thừa, gồm Schurrle, Luiz, Juan Mata và Romelu Lukaku, với tổng số tiền lên tới khoảng 213 triệu đôla. Đó là điều đáng kinh ngạc, và quan trọng hơn nó cho phép họ không chỉ tăng cường được sức mạnh hàng công bằng việc mang về ngôi sao tiền đạo Diego Costa từ Atletico Madrid, mà còn giúp họ dư tiền giữ chân các trụ cột khác, chẳng hạn như Hazard.
Nhưng Marina Granovskaia không chỉ đứng đầu trong các vụ đàm phán mua bán cầu thủ quan trọng của Chelsea những năm gần đây. Người phụ nữ quyền lực này còn đóng vai trò chính trong nỗ lực mang Jose Mourinho trở lại sân Stamford Bridge.
CLB phía tây London từng nhiều lần phải đối mặt với khó khăn trong việc cân đối thu chi để không vi phạm Đạo luật Công bằng Tài chính giữa các đội bóng. Nhưng cuối cùng họ đều thoát hiểm nhờ quyết định tập trung mua sắm cầu thủ theo chiến lược chỉ gồm hai phần rõ ràng.
Marina (giữa) rất chịu khó nghiên cứu và là người vạch chiến lược mua sắm cầu thủ cho Chelsea.
Thứ nhất, Chelsea tập trung vào chất lượng đội hình, nghĩa là họ chấp nhận có ít cầu thủ hơn một số đối thủ nhưng có thể dùng tốt 8 trong số mỗi 10 cầu thủ còn hơn để xảy ra tình trạng dư thừa nhiều người. Vụ mua Cuadrado mới đây dẫn tới sự ra đi của Mohamed Salah cũng như Schurrle. Và điều đó diễn ra theo đúng chính sách của họ: nâng cấp khả năng đội hình, nhưng không bị lỗ tiền.
Thứ hai, Chelsea tìm kiếm mua các cầu thủ trẻ tài năng, rồi giúp họ tiếp tục phát triển sự nghiệp (chủ yếu bằng hình thức cho các đội khác mượn) và sau đó bán khi được giá hoặc đôn họ lên thi đấu ở đội một nếu đủ khả năng. Điển hình cho chính sách này là hai vụ liên quan tới các tài năng trẻ người Bỉ. Chelsea mua tiền vệ tấn công 23 tuổi Kevin de Bruyne hồi tháng 1/2012 với giá 10 triệu đôla, sau đó đem cho mượn trước khi bán đứt cho Wolfsburg tháng 1/2014 với giá lên tới 27 triệu đôla. Trong khi đó, Thibaut Courtois hiện được đánh giá là một trong những thủ môn hàng đầu thế giới. Anh mới trở lại Chelsea từ mùa hè 2014, sau khi thi đấu ở Atletico Madrid trong ba mùa giải theo diện cho mượn.