Real trả giá đắt cho chiến thắng
James Rodriguez có thể phải nghỉ thi đấu đến hai tháng, còn Sergio Ramos nhiều khả năng không thể ra sân trong trận derby thành Madrid cuối tuần này.
Real thắng trận thứ hai liên tiếp mà không cần Ronaldo
James Rodriguez dính chấn thương nặng ở bàn chân.
Hai ca chấn thương đều xảy ra trong trận Real thắng Sevilla tối 4/2. Đây là trận đấu bù của thầy trò HLV Carlo Ancelotti cho vòng đấu mà họ vắng mặt khi tới Marốc dự FIFA Club World Cup cuối năm ngoái.
James Rodriguez phải rời sân trong hiệp đầu sau một pha va chạm ở chân phải. Trong cuộc kiểm tra tại bệnh viện, các bác sĩ xác định Vua phá lưới World Cup 2014 bị gãy xương bàn chân và có thể phải nghỉ thi đấu đến hai tháng.
Sergio Ramos thậm chí rời sân sớm hơn James khi dính chấn thương gân kheo ở phút thứ chín. Khả năng trung vệ này bình phục cho chuyến làm khách trên sân Vicente Calderon của Atletico Madrid cuối tuần này đang đứng trước một dấu hỏi.
Hai trận gần nhất Real đều không có sự phục vụ của Ronaldo do án treo giò. Trong hai trận này James đều lập công và đem lại chiến thắng.
Trong khi Ronaldo sẽ trở lại để chơi thay James, sự vắng mặt của Ramos có thể khiến HLV Ancelotti đau đầu hơn. Real hiện chỉ có Raphael Varane và Nacho Fernandez cho vị trí trung vệ. Pepe vẫn chưa bình phục chấn thương.
Hàng thủ Real trận tới cũng sẽ thiếu sự phục vụ của Marcelo. Hậu vệ trái này vừa nhận thẻ vàng và đủ số thẻ để ngồi ngoài một trận.